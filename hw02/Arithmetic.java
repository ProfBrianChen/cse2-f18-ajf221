///////////////
/// CSE02 Arithhmetic hw02
/// Ann Foley
/// 9/11/18
/// Section 110

//Compute the cost of items bought including a PA sales tax of 6% 
public class Arithmetic {
  public static void main(String args[]){
    
// input variables
    //Number of pairs of pants 
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    
    //Number of belts 
    int numBelts = 1;
    //Cost per belt 
    double beltCost = 33.99;
    
    //PA tax rate 
    double paSalesTax = 0.06;
    
   //Caculates the cost of the item without tax, the amount of tax on the item, 
    //and then the total cost of item listed
    
    //pants
    double costOfPantsNoTax = numPants * pantsPrice;
    //For the rest of the program, I will multiply my answers by 
    // 100, convert them to integers, and divide by 100.0 to ensure 2 decimal places
    double taxOnPants = (int) ((paSalesTax * costOfPantsNoTax) * 100) / 100.0;
    double totalCostOfPants = costOfPantsNoTax + taxOnPants;
    
    //sweatshits
    double costOfShirtsNoTax = numShirts * shirtPrice;
    double taxOnShirts =(int) ((paSalesTax * costOfShirtsNoTax) * 100) / 100.0;
    double totalCostOfShirts = costOfShirtsNoTax + taxOnShirts;
    
    
    //belts
    double costOfBeltsNoTax = numBelts * beltCost;
    double taxOnBelts = (int) ((paSalesTax * costOfBeltsNoTax) * 100) / 100.0 ;
    double totalCostOfBelts = taxOnBelts + costOfBeltsNoTax;
    
    // Calculates the total cost of all the purchases combined
    // Calculates the total sales tax paid
    
    double totalCostOfItems = (int) ((totalCostOfBelts + totalCostOfShirts + totalCostOfPants) * 100) / 100.0;
    double totalSalesTax = taxOnBelts + taxOnPants + taxOnShirts;
    
    // Prints the cost of the item before tax, the total sales tax on the item, and the total cost of the purchase
    //pants
    System.out.println("The cost of the pants before tax is " + costOfPantsNoTax);
    System.out.println("The total tax on the pants is " + taxOnPants);
    System.out.println("The total cost of the pants including tax is " + totalCostOfPants);
    System.out.println(" "); //prints space for clarity and readability 
    
    //shirts
    System.out.println("The cost of the shirts before tax is " + costOfShirtsNoTax);
    System.out.println("The total tax on the shirts is " + taxOnShirts);
    System.out.println("The total cost of the shirts including tax is " + totalCostOfShirts);   
    System.out.println(" ");
    
    //belts
    System.out.println("The cost of the belts before tax is " + costOfBeltsNoTax);
    System.out.println("The total tax on the belts is " + taxOnBelts);
    System.out.println("The total cost of the belts including tax is " + totalCostOfBelts);
    System.out.println(" ");
    
    // Prints the total cost including tax of all the items combined and Sales Tax on all the Purchases
    System.out.println("The total tax on all the items is " + totalSalesTax);
    System.out.println("The total cost of the items including tax is " + totalCostOfItems);
    System.out.println(" ");
    
    
  }
}