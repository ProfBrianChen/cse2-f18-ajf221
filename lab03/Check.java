///////////////
/// CSE02 Check Lab03
/// Ann Foley
/// 9/14/18
/// Section 110

/*The user has gone out to dinner with friends. 
After they receive the bill, they decide to split the check evenly. 
Write a program that uses the Scanner class to obtain from the user the original
cost of the check, the percentage tip they wish to pay,
and the number of ways the check will be split. 
Then determine how much each person in the group needs to spend in order to pay the check. */

import java.util.Scanner; //imports Scanner class, otherwise we would receive errors


public class Check{
    	// main method required for every Java program
      public static void main(String[] args) {
        
        //the following statement constructs the instance of the Scanner declared (i.e. myScanner)
        // and allows the user to input a value
        Scanner myScanner = new Scanner( System.in ); 
        
        //CHECK
        //prompts for original cost of check
        System.out.print("Enter the original cost of the check in the form xx.xx: ");
        double checkCost = myScanner.nextDouble(); // accepts user input, declares it as a double
        
        //TIP
        //prompts for input of tip
        System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
        double tipPercent = myScanner.nextDouble(); //accepts tip value as a double
        tipPercent /= 100; //We want to convert the percentage into a decimal value
        
        //prompts for number of people, and accepts input as an integer 
        //equal to the number of ways the check will be split 
        System.out.print("Enter the number of people who went out to dinner: ");
        int numPeople = myScanner.nextInt();
        
        // Declares variables to be used in calculations 
        double totalCost;
        double costPerPerson;
        int dollars,   //whole dollar amount of cost 
            dimes, pennies; //for storing digits to the right of the decimal point for the cost$ 
        
        //CALCULATIONS
        totalCost = checkCost * (1 + tipPercent); // calculates total cost
        costPerPerson = totalCost / numPeople; //get the whole amount of cost per person, dropping decimal fraction
        dollars=(int)costPerPerson; 
              /*get dimes amount, e.g., 
              (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
              where the % (mod) operator returns the remainder
              after the division:   583%100 -> 83, 27%5 -> 2 */
        dimes=(int)(costPerPerson * 10) % 10; //covers dime amounts
        pennies=(int)(costPerPerson * 100) % 10; //covers penny amount

        //prints the final answer
        System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);

}  //end of main method   
  	} //end of clas