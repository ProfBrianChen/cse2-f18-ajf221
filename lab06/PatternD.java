///////////////
/// CSE02 PatternD Lab06
/// Ann Foley
/// 10/12/18
/// Section 110

import java.util.Scanner;
  public class PatternD{
    public static void main(String[] args){
     
      Scanner myScanner = new Scanner( System.in );
  
      int intTest = 1; //declares a test variable to enter/exit the while statements  
      int numRows = 0;
      
      //tests user input
      while(intTest < 2){
     
      System.out.println("Enter an integer 1-10: ");
      boolean correctInt = myScanner.hasNextInt(); //checks to make sure the input is an integer
      
        if (correctInt){
          numRows = myScanner.nextInt(); 
        }
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer from 1-10");
        }//ends else
        
        
          if (numRows > 0 && numRows < 11){  
            intTest++; //increments test value to leave while statement
         }
          
          else  {
            myScanner.next();
            System.out.println("Error invalid input, please enter an integer from 1-10");
         }//en s else 
      
     }//ends while
      
      
      for (int columns = numRows; columns >= 1; columns--){ //number of columns decreasing
        //starts each row with highest number (ie the numRows input)
        //decrements r as the row progresses
          for (int r = columns; r >= 1; r--){ //rows
            System.out.print(r + " ");
        }
        System.out.println(); //new line between rows
        }// ends big for

      
    }//ends main
    
  }//ends class