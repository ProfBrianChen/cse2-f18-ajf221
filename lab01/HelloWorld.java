///////////////
//// CSE02 Hello World
/// Ann Foley
/// 8/31/18
/// Section 110
public class HelloWorld{
  
  public static void main(String args[]){
    ///prints Hello, World to terminal window
    System.out.println("Hello, World");
  }
}