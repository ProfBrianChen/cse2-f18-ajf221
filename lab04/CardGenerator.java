///////////////
/// CSE02 Card Generator Lab04
/// Ann Foley
/// 9/21/18
/// Section 110

/* You are a magician and you need to practice your card tricks before 
your big show in Las Vegas. Since you do not want to reveal your secrets, 
you need to write a program that will pick a random card from the deck 
so you can practice your tricks alone. Use a random number generator to 
select a number from 1 to 52 (inclusive).  Each number represents one card, 
and the suits are grouped:  Cards 1-13 represent the diamonds, 14-26 represent the clubs, 
then hearts, then spades.  In all suits, card identities ascend in step with the card number: 
14 is the ace of clubs, 15 is the 2 of clubs, and 26 is the king of clubs. */

/* 
1. After you generate a random number, create two String variables: 
a String corresponding to the name of the suit and a String corresponding to the identity of the card.
2. Use if statements to assign the suit name.
3. Use a switch statement to assign the card identity.
4. Print out the name of the randomly selected card. */

import java.util.Random; //imports random

public class CardGenerator{
  public static void main(String[] args){
       
    Random randGen = new Random();//new random number generator
    
    int card = randGen.nextInt(13) + 1; //generates a random number 1-13 for the variable card
    int suit = randGen.nextInt(4) + 1; //generates a random number 1-4 for the variable suit
    
    
  //switch suit numbers to words  
      String suitString = "undefined suit";
       switch (suit) {
         case 1: suitString = "Diamonds";
           break; 
          case 2: suitString = "Clubs"; 
           break; 
         case 3: suitString = "Hearts";
           break;
         case 4: suitString = "Spades";
          break;
       } //ends switch

    // switches card numbers to words
    String cardString = "undefined card";
    switch (card) {
      case 1: cardString = "Ace of ";
        break;
      case 2: cardString = "2 of ";
        break;
      case 3: cardString = "3 of ";
        break;
      case 4: cardString = "4 of ";
        break;
      case 5: cardString = "5 of ";
        break;
      case 6: cardString = "6 of ";
        break;
      case 7: cardString = "7 of ";
        break;
      case 8: cardString = "8 of ";
        break;
      case 9: cardString = "9 of ";
        break;
      case 10: cardString = "10 of ";
        break;
      case 11: cardString = "Jack of ";
        break;
      case 12: cardString = "Queen of ";
        break;
      case 13: cardString = "King of ";
        break;  
    }//ends card switch

    //prints the final statment including card type and suit
    System.out.println("You picked the " + cardString + suitString); 

  } //ends main
  
} //ends class method
