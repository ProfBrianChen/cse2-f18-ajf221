 ///////////////
/// CSE02 Remove Elements HW09
/// Ann Foley
/// 11/27/18
/// Section 110

/*main method given, must write 3 methods:
The randomInput() method generates an array of 10 random integers between 0 to 9.  
It fills the array with random integers and returns the filled array. 

The method delete(list,pos) takes, as input, an integer array called list and 
an integer called pos. It should create a new array that has one member fewer than list, 
and be composed of all of the same members except the member in the position pos. 

Method remove(list,target) deletes all the elements that are 
equal to target, returning a new list without all those new elements.
My program does not delete targets if they are right next to each other.

*/
import java.util.Scanner;
import java.util.Random;

public class RemoveElements{
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);

   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
      System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }//ends main
 
  public static String listArray(int num[]){
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }//ends listArray
  
  public static int[] randomInput(){
    int [] arrayRand = new int[15];
    Random randGen = new Random();  
      for(int i=0; i < arrayRand.length; i++){
        int input = randGen.nextInt(10);//generates random numbers 0-9
        arrayRand[i] = input;
      }//ends for
   return arrayRand;
  }//ends randomInput
  
  public static int[] delete(int[] list, int pos){
    //shifts elements to the right of pos
      for(int j = pos; j < list.length-1; j++){
       list[j] = list[j+1];
      }  
    //creates new array without the inputed index element
    int[] listDelete = new int[(list.length - 1)];
    for(int i=0; i <(list.length - 1); i++){
      listDelete[i] = list[i];
    }
    return listDelete;
  }//ends delete

  public static int[] remove(int[] list, int target){
    int counter = 1;
    for(int i = 0; i < list.length; i++){//goes through entire array
      if(list[i] == target){
        //does not delete dublicates, the program kept getting messed up when I tried
        for(int j = i; j < list.length-1; j++){//shifts elements starting at i to the left, length-1 so we don't go out of bounds
          list[j] = list[j+1];//takes elements and replaces with the one after to shorten array
        }
        counter++;//counts for the number of elements removed
      }
    }
    //creates revised array without the targets and adjusts the length
    int[] listRemove = new int[(list.length - counter)];
    for(int i=0; i <(list.length - counter); i++){
      listRemove[i] = list[i];
    }
    return listRemove;
  }//ends remove
    
  
}//ends class
 
