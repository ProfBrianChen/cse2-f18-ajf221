///////////////
/// CSE02 Welcome Class hw01
/// Ann Foley
/// 9/4/18
/// Section 110
public class WelcomeClass{
public static void main(String args[]){
  //prints Welcome
  System.out.println(" -----------");
  System.out.println("|  WELCOME  |");
  System.out.println(" -----------");
  //prints Lehigh ID with decoration
  System.out.println("  ^  ^  ^  ^  ^  ^");
  System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
  System.out.println("<-A--J--F--2--2--1->");
  System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println("  v  v  v  v  v  v");
  //prints short bio
  System.out.println("I'm a sophomore engineering student and I'm a sprint freestyler on the swim team");
                     }
                     }
                   