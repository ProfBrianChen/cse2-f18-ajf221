///////////////
/// CSE02 PatternA Lab06
/// Ann Foley
/// 10/12/18
/// Section 110

//Coded using nested loops, displays pattern based on user input, eg. if user enters 4
/* 
1      	        	
1 2
1 2 3
1 2 3 4 */


import java.util.Scanner;
  public class PatternA{
    public static void main(String[] args){
     
      Scanner myScanner = new Scanner( System.in );
  
      int intTest = 1; //declares a test variable to enter/exit the while statements  
      int numRows = 0;
      
      //tests user input
      while(intTest < 2){
     
      System.out.println("Enter an integer 1-10: ");
      boolean correctInt = myScanner.hasNextInt(); //checks to make sure the input is an integer
      
        if (correctInt){
          numRows = myScanner.nextInt(); 
        }
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer from 1-10");
        }//ends else
        
        
          if (numRows > 0 && numRows < 11){  
            intTest++; //increments test value to leave while statement
         }
          
          else  {
            myScanner.next();
            System.out.println("Error invalid input, please enter an integer from 1-10");
         }//en s else 
      
     }//ends while
      
      //enters for loop, first loop runs so that there are 
      //numRows (input value) number of rows
      for (int r = 1; r <= numRows; r++){ //rows
        //starts columns with a one and incriments by one until it reaches 
        //the value of r
        for (int columns = 1; columns <= r; columns++){
            System.out.print(columns + " ");
        }
        System.out.println(); //prints new line between rows
        }// ends big for


      
    }//ends main
    
  }//ends class