///////////////
/// CSE02 PatternC Lab06
/// Ann Foley
/// 10/12/18
/// Section 110

//Coded using nested loops, displays pattern based on user input, backwards right alligned pyramid

import java.util.Scanner;
  public class PatternC{
    public static void main(String[] args){
     
      Scanner myScanner = new Scanner( System.in );
  
      int intTest = 1; //declares a test variable to enter/exit the while statements  
      int numRows = 0;
      
      //checks if user input is correct
      while(intTest < 2){
     
      System.out.println("Enter an integer 1-10: ");
      boolean correctInt = myScanner.hasNextInt(); //checks to make sure the input is an integer
      
        if (correctInt){
          numRows = myScanner.nextInt(); 
        }
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer from 1-10");
        }//ends else
        
        
          if (numRows > 0 && numRows < 11){  
            intTest++; //increments test value to leave while statement
         }
          
          else  {
            myScanner.next();
            System.out.println("Error invalid input, please enter an integer from 1-10");
         }//en s else 
      
     }//ends while
      
      // enters for loop
      for (int r = 1; r <= numRows; r++){ //rows
        //produces spaces so the numbers are right alligned
        for(int spaces = 1; spaces <= numRows - r; spaces++){
          System.out.print(" ");
        }
       // makes column = row so that the pyramid starts with the biggest #
        //decrements to 1
        for (int columns = r; columns >= 1; columns--){ //rows
            System.out.print(columns);
        }
        System.out.println(); //prints new line between rows
        }// ends big for


      
    }//ends main
    
  }//ends class