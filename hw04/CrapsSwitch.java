///////////////
/// CSE02 Craps Switch HW04
/// Ann Foley
/// 9/25/18
/// Section 110

// The classic casino game, Craps, involves the rolling of two six sided dice and evaluating the result of the roll.  
//Among other things, craps is notable for producing slang terminology for describing the outcome of a roll of two dice.
//In this program, the user will either randomly roll 2 dice or select numbers from 2 dice. 
//Then, using if statements, the program will produce the proper slang from the game Craps based on the rolls

import java.util.Random; //imports random calculator 
import java.util.Scanner; //imports scanner

public class CrapsSwitch{
public static void main(String[] args) {

Random randGen = new Random(); //random
Scanner myScanner = new Scanner( System.in );//scanner

//initializes the variables for the dice rolls
int diceRoll1 = 0;
int diceRoll2 = 0; 

  //prompts user input
System.out.println("Please select 1 if you would you like to randomly roll dice or select 2 to choose the numbers: ");
int selection = myScanner.nextInt(); //casts imput as an integer with a variable 

  String selectionString = "undefined";
  switch(selection){
    case 2: selectionString = "Please enter a number from 1 to 6: ";
      //first number prompt
      System.out.println(selectionString);
      diceRoll1 = myScanner.nextInt();
      
      //second number prompt
      System.out.println(selectionString);
      diceRoll2 = myScanner.nextInt();
      
      break;
      
    case 1: //selectionString = "";
      diceRoll1 = randGen.nextInt(6) + 1; //generates a random number 1-6 for first roll
      diceRoll2 = randGen.nextInt(6) + 1; //generates a random number 1-6 for second roll
      System.out.println("The first die is " + diceRoll1 + " the second die is " + diceRoll2);
      break;
   
  }//ends switch
  
  //creates variable sum to use in the switch statement 
 int sum = diceRoll1 + diceRoll2;

//initializes string variables to use in switch statements 
String diceRoll1String = "undefined";
String sumString = "undefined";
  switch (sum){
      //if the sum is 2, the outcome will be "Snake Eyes"
      //the same goes for each case and outcome in quotes
    case 2: sumString = "Snake Eyes"; 
      break;
    case 3: sumString = "Ace Duece";
      break;
    case 4: sumString = "Easy Four";
      //this switch statement accounts for 2+2 being a hard four
      //if the first dice is a 2, it will enter this switch statement and the 
      //outcome will be "Hard Four" rather than "Easy Four"
      //the same goes for the other "Hard" sums and switch statements 
      switch (diceRoll1){
        case 2: diceRoll1String = "Hard Four";  
                sumString = diceRoll1String;
          break;
      } //ends switch for 4
      
      break;
    case 5: sumString = "Fever Five";
      break;
    case 6: sumString = "Easy Six";
      
      switch (diceRoll1){
        case 3: diceRoll1String = "Hard Six";
                sumString = diceRoll1String;
          break;
      } //ends switch for 6
          
     break;
          
    case 7: sumString = "Seven out";
      break;
    case 8: sumString = "Easy Eight";
      
      switch(diceRoll1){
        case 4: diceRoll1String = "Hard Eight";
                sumString = diceRoll1String;
          break;
      } //ends switch for 8
     break;
      
    case 9: sumString = "Nine";
      break;
    case 10: sumString = "Easy Ten";
      
      switch(diceRoll1){
        case 5: diceRoll1String = "Hard Ten";
                sumString = diceRoll1String;
          break;
      } //ends switch for 10
      break; //break for case 20
      
    case 11: sumString = "Yo-leven";
      break;
    case 12: sumString = "Boxcars";
      
  }//ends switch
  
  //prints slang
 System.out.println(sumString);
  
}//ends main
}//ends class