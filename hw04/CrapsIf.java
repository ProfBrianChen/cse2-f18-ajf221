///////////////
/// CSE02 Craps If HW04
/// Ann Foley
/// 9/25/18
/// Section 110

// The classic casino game, Craps, involves the rolling of two six sided dice and evaluating the result of the roll.  
//Among other things, craps is notable for producing slang terminology for describing the outcome of a roll of two dice.
//In this program, the user will either randomly roll 2 dice or select numbers from 2 dice. 
//Then, using if statements, the program will produce the proper slang from the game Craps based on the rolls

import java.util.Random; //imports Random and Scanner
import java.util.Scanner;

public class CrapsIf{
public static void main(String[] args) {

Random randGen = new Random(); //random
Scanner myScanner = new Scanner( System.in );//scanner
  
int diceRoll1 = 0;
int diceRoll2 = 0;

System.out.println("Please select 1 if you would you like to randomly roll dice or select 2 to choose the numbers: ");
int selection = myScanner.nextInt(); //casts imput as an integer with a variable 

//to generate a random number based on user input
if (selection == 1){
  diceRoll1 = randGen.nextInt(6) + 1; //generates a random number 1-6 for first roll
  diceRoll2 = randGen.nextInt(6) + 1; //generates a random number 1-6 for second roll
  System.out.println("The first die is " + diceRoll1 + " the second die is " + diceRoll2);
  
 }//if statement
  
else if (selection == 2){
  //promts user for first input
  System.out.println("Please enter a number from 1 to 6: ");
  diceRoll1 = myScanner.nextInt();
  
  //prompts for second input
  System.out.println("Please enter a second number from 1 to 6: ");
  diceRoll2 = myScanner.nextInt();
  
  //if statement in case the user enters an invalid number 
  if (diceRoll1 < 1 || diceRoll1 > 6 || diceRoll2 < 1 || diceRoll2 > 6){
    System.out.println("Your selection is invalid. The numbers must be from 1 to 6");
    }//ends if 
  }//else if statement 
  
else { // in case the user inputs a value other than 1 or 2
  System.out.println("Your selection is invalid");
}//ends else

  // each dice roll is accounted for in the following if statements 
  
if (diceRoll1 == 1){
  if (diceRoll2 == 1){
    System.out.println("Snake eyes");
  }
  else if (diceRoll2 == 2){
    System.out.println("Ace Deuce");
  }
  else if (diceRoll2 == 3){
    System.out.println("Easy Four");
  }
  else if (diceRoll2 == 4){
    System.out.println("Fever Five");
  }
  else if (diceRoll2 == 5){
    System.out.println("Easy Six");
  }
  else if (diceRoll2 == 6){
    System.out.println("Seven out");
  }
}//ends main if statement for rolling a 1 on the first die

if (diceRoll1 == 2){
   if (diceRoll2 == 1){
    System.out.println("Ace Deuce");
  }
  else if (diceRoll2 == 2){
    System.out.println("Hard Four");
  }
  else if (diceRoll2 == 3){
    System.out.println("Fever Five");
  }
  else if (diceRoll2 == 4){
    System.out.println("Easy Six");
  }
  else if (diceRoll2 == 5){
    System.out.println("Seven out");
  }
  else if (diceRoll2 == 6){
    System.out.println("Easy Eight");
  }
}//ends main if statement for rolling a 2 on the first die

if (diceRoll1 == 3){
  
  if (diceRoll2 == 1){
    System.out.println("Easy Four");
  }
  else if (diceRoll2 == 2){
    System.out.println("Fever Five");
  }
  else if (diceRoll2 == 3){
    System.out.println("Hard six");
  }
  else if (diceRoll2 == 4){
    System.out.println("Seven out");
  }
  else if (diceRoll2 == 5){
    System.out.println("Easy Eight");
  }
  else if (diceRoll2 == 6){
    System.out.println("Nine");
  }
  
}//ends main if statement for rolling a 3 on the first die

if (diceRoll1 == 4){
  if (diceRoll2 == 1){
    System.out.println("Fever Five");
  }
   else if (diceRoll2 == 2){
     System.out.println("Easy Six");
   }
   else if (diceRoll2 == 3){
    System.out.println("Seven out");
  }
  else if (diceRoll2 == 4){
    System.out.println("Hard Eight");
  }
  else if (diceRoll2 == 5){
    System.out.println("Nine");
  }
  else if (diceRoll2 == 6){
     System.out.println("Easy Ten");
  }
  
}//ends main if statement for rolling a 4 on the first die

if (diceRoll1 == 5){
  if (diceRoll2 == 1){
    System.out.println("Easy Six");
  }
  else if (diceRoll2 == 2){
    System.out.println("Seven out");
  }
  else if (diceRoll2 == 3){
    System.out.println("Easy Eight");
  }
  else if (diceRoll2 == 4){
    System.out.println("Nine");
  }
  else if (diceRoll2 == 5){
     System.out.println("Hard Ten");
  }
  else if (diceRoll2 == 6){
    System.out.println("Yo-leven");
  } 
  
}//ends main if statement for rolling a 5 on the first die

if (diceRoll1 == 6){
  if (diceRoll2 == 1){
    System.out.println("Seven out");
  }
  else if (diceRoll2 == 2){
    System.out.println("Easy Eight");
  }
  else if (diceRoll2 == 3){
    System.out.println("Nine");
  }
  else if (diceRoll2 == 4){
     System.out.println("Easy Ten");
  }
  else if (diceRoll2 == 5){
    System.out.println("Yo-leven");
  } 
  else if (diceRoll2 == 6){
    System.out.println("Boxcars");
  }
  
}//ends main if statement for rolling a 6 on the first die
// I realize this program could've been written with comparisons 
  //however although this is a repetative program I found it most comfortable 
}//ends main

}//ends class