/* SCANNERS
9/12/18

in order to use an object, you must do 3 things
  1. Import the class
  2. Declare an instance of the object
  3. Contruct the instance
  
  import.java.util.Scanner; //import (before the class!)
  Scanner myScanner; //declare (within the main method!)
  myScanner = new Scanner(System.in); //construct 
  
  // taking user import in the form of an integer, see oracle document (nextInt)
  */

import java.util.Scanner; //muat go before the class

public class scannerIntroduction{
	public static void main(String[] args){
		Scanner myScanner; //declares
		
		myScanner = new Scanner(System.in); //construct
		// In order:
		//(name of the instance) = ("new" operator) (Constructor) (input for the constructor)
		// The "input" System.in tells the compiler what 
		//   you're going to do with your Scanner (here,
		//   we're going to receive input from the screen)
		
		// Ask the user to input an integer
		System.out.println("Please input an integer A: ");
		// nextInt() allows the user to input an integer
    //when you input an integer, you cannot divide (i.e. put in 10/2) or put in words
		int integerA = myScanner.nextInt();

		// Ask the user to input another integer
		System.out.println("Please input an integer B: ");
		int integerB = myScanner.nextInt();
          
		// Add the two integers and output them to the screen 
		System.out.println("A + B = ");
		System.out.println(integerA + integerB);
		myScanner.nextLine(); // Ignore the remainder of the
		                      // line so we can input other text
		
		// Ask the user to input her name
		System.out.println("Now please enter your name: ");
		// nextLine() allows the user to input a string
    // nextLine allows you to write anything until you press enter
		String myName = myScanner.nextLine();
		
		System.out.println("Hello, " + myName);
		
	} //main
	
} //class


 
 



  