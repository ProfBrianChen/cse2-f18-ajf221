///////////////
/// CSE02 While Loops HW05
/// Ann Foley
/// 10/10/18
/// Section 110

//5 card poker hand
//calculates the probability of one pair, two pair, three of a kind, and four of a kind
//given a certain number of hands

import java.util.Scanner;
import java.util.Random;
  public class WhileLoops{
    public static void main(String[] args){
     
      Scanner myScanner = new Scanner( System.in );
      
      
      int intTest = 1;
      int countHands = 0;
      int numHands = 0;
      int onePair = 0;
      int twoPair = 0;
      int threeOfKind = 0;
      int fourOfKind = 0;
      int card1 = 0;
      int card2 = 0;
      int card3 = 0;
      int card4 = 0;
      int card5 = 0;
      
      
      //Prompts for integer for number of hands 
      //tests if correct and assigns value or gives error
      while(intTest < 2){
     
      System.out.println("how many hands of poker do you want to generate? ");
      boolean correctInt = myScanner.hasNextInt();
      
        if (correctInt){
          numHands = myScanner.nextInt();
          intTest++;
        
      }//ends if
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer");
      }//ends else
      
      }//ends while
      
      
      while (countHands < numHands){
        
        Random randGen = new Random(); //random number generator
        card1 = randGen.nextInt(52) + 1; //generates a random number 1-52
        card2 = randGen.nextInt(52) + 1; //generates a random number 1-52
        card3 = randGen.nextInt(52) + 1; //generates a random number 1-52
        card4 = randGen.nextInt(52) + 1; //generates a random number 1-52
        card5 = randGen.nextInt(52) + 1; //generates a random number 1-52
        
        //System.out.println(card1 + " " + card2 + " " + card3 + " " + card4 + " " + card5);
        //checks for cards equalling
        
        while (card1 == card2){
          card2 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while card1 and 2
        
        while(card1 == card3){
          card3 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while
        
        while (card1 == card4){
          card4 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while 
        
        while(card1 == card5){
          card5 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while
        
        while(card2 == card3){
         card3 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while
        
        while(card2 == card4){
         card4 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while
        
        while(card2 == card5){
         card5 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while
        
        while(card3 == card4){
         card4 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while
        
        while(card3 == card5){
         card5 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while
        
        while(card4 == card5){
         card5 = randGen.nextInt(52) + 1; //generates a random number 1-52
        }//ends while
         
        //mod to change all #s to 1-13
        card1 = card1 % 13; 
        card2 = card2 % 13;
        card3 = card3 % 13;
        card4 = card4 % 13;
        card5 = card5 % 13;
        
        //System.out.println(card1 + " " + card2 + " " + card3 + " " + card4 + " " + card5);
        
        //FOUR OF A KIND (5 ways)
       //1234
        if (card1 == card2 && card2 == card3 && card3 == card4 && card1 != card5){
          fourOfKind++;
         // System.out.println("If statement entered");
        }//ends if 
        
        //2345
        if (card2 == card3 && card3 == card4 && card4 == card5 && card2 != card1){
          fourOfKind++;
          //System.out.println("If statement entered");
        }//ends if 
        
        //3451
        if (card3 == card4 && card4 == card5 && card5 == card1 && card3 != card2){
          fourOfKind++;
          //System.out.println("If statement entered");
        }//ends if 
        
        //4512
        if (card4 == card5 && card5 == card1 && card1 == card2 && card4 != card3){
          fourOfKind++;
          //System.out.println("If statement entered");
        }//ends if 
        
        //5123
        if (card5 == card1 && card1 == card2 && card2 == card3 && card5 != card4){
          fourOfKind++;
        }//ends if 
        
        //THREE OF A KIND (10 ways) 
        
        //123
         if (card1 == card2 && card2 == card3 && card1 != card4 && card1 != card5){
          threeOfKind++;
        }//ends if 
         
         //124
         if (card1 == card2 && card2 == card4 && card1 != card5 && card1 != card3){
          threeOfKind++;
        }//ends if
         
         //125
         if (card1 == card2 && card2 == card5 && card1 != card3 && card1 != card4){
          threeOfKind++;
        }//ends if
        
         //234
        if (card2 == card3 && card3 == card4 && card2 != card1 && card2 != card5){
          threeOfKind++;
        }//ends if
        
        //235
        if (card2 == card3 && card3 == card5 && card2 != card1 && card2 != card4){
          threeOfKind++;
        }//ends if
        
        //245
        if (card2 == card4 && card4 == card5 && card2 != card3 && card2 != card1){
          threeOfKind++;
        }//ends if
        
        //345
        if (card3 == card4 && card4 == card5 && card3 != card1 && card3 != card2){
          threeOfKind++;
        }//ends if
        
        //341
        if (card3 == card4 && card4 == card1 && card3 != card5 && card3 != card2){
          threeOfKind++;
        }//ends if
        
        //351
        if (card3 == card5 && card5 == card1 && card3 != card2 && card3 != card4){
          threeOfKind++;
        }//ends if
        
        //451
        if (card4 == card5 && card5 == card1 && card4 != card2 && card4 != card3){
          threeOfKind++;
        }//ends if
        
  //TWO PAIR
        
        //first 2 cards equal, first cards don't equal last; second cards equal, second cards do not equal last
        //first cards do not equal second cards
        
         //12 34 //5
        if (card1 == card2 && card1 != card5 && card3 == card4 && card3 != card5 && card1 != card3){
            twoPair++;
          }
          
          //12 35 //4
          if (card1 == card2 && card1 != card4 && card3 == card5 && card3 != card4 && card1 != card3){
            twoPair++;
          }
          
          //12 45 //3
          if (card1 == card2 && card1 != card3 && card4 == card5 && card3 != card4 && card1 != card4){
            twoPair++;
          }
        
         //13 24 //5
         if (card1 == card3 && card1 != card5 && card2 == card4 && card2 != card5 && card1 != card2){
            twoPair++;
          }
          
          //13 25 //4
          if (card1 == card3 && card1 != card4 && card2 == card5 && card2 != card4 && card1 != card2){
            twoPair++;
          }
          
          //13 45 //2
          if (card1 == card3 && card1 != card2 && card4 == card5 && card2 != card4 && card1 != card4){
            twoPair++;
          }
          
         //14 32 //5
          if (card1 == card4 && card1 != card5 && card3 == card2 && card3 != card5 && card1 != card3){
            twoPair++;
          }
          
          //14 35 //2
          if (card1 == card4 && card1 != card2 && card3 == card5 && card3 != card4 && card1 != card3){
            twoPair++;
          }
          
          //14 25 //3
         if (card1 == card4 && card1 != card3 && card2 == card5 && card2 != card4 && card1 != card2){
            twoPair++;
          }
         
         //15 32 //4
         if (card1 == card5 && card1 != card4 && card2 == card3 && card2 != card4 && card1 != card3){
            twoPair++;
          }
         
         //15 34 //2
         if (card1 == card5 && card1 != card2 && card4 == card3 && card2 != card4 && card1 != card3){
            twoPair++;
          }
         
         //15 24 //3
         if (card1 == card5 && card1 != card3 && card2 == card4 && card2 != card3 && card1 != card4){
            twoPair++;
          }
         
         //23 45 
         if (card2 == card3 && card2 != card1 && card5 == card4 && card4 != card1 && card2 != card4){
            twoPair++;
          }
         
         //24 35 
         if (card2 == card4 && card2 != card1 && card3 == card5 && card3 != card1 && card2 != card3){
            twoPair++;
          }
         
          //25 34 
         if (card2 == card5 && card2 != card1 && card3 == card4 && card3 != card1 && card2 != card3){
            twoPair++;
          }
         
         //ONE PAIR
         
         //12
        if (card1 == card2 && card1 != card3 && card1 != card4 && card1 != card5 && card3 != card4 && card3 != card5 && card4 != card5){
            onePair++;   
          }
        
        //13
        if (card1 == card3 && card1 != card2 && card1 != card4 && card1 != card5 && card2 != card4 && card2 != card5 && card4 != card5){
            onePair++;   
          }
        
        //14
        if (card1 == card4 && card1 != card2 && card1 != card3 && card1 != card5 && card2 != card3 && card2 != card5 && card3 != card5){
            onePair++;   
          }
        
        //15 //234
        if (card1 == card5 && card1 != card2 && card1 != card3 && card1 != card4 && card2 != card3 && card2 != card4 && card3 != card4){
            onePair++;   
          }
        
        //23 //145
        if (card2 == card3 && card2 != card1 && card2 != card4 && card2 != card5 && card1 != card4 && card1 != card5 && card4 != card5){
            onePair = onePair + 1;  
          //System.out.println("If statement entered");
          }
        
        
        //24 //135
        if (card2 == card4 && card2 != card1 && card2 != card3 && card2 != card5 && card1 != card3 && card1 != card5 && card3 != card5){
            onePair++;   
          }
         
        //25 //134
        if (card2 == card5 && card2 != card1 && card2 != card3 && card2 != card4 && card1 != card3 && card1 != card4 && card3 != card4){
            onePair++;   
          }

        
        //34 //125
        if (card3 == card4 && card3 != card1 && card3 != card2 && card3 != card5 && card1 != card2 && card1 != card5 && card2 != card5){
            onePair++;   
          //System.out.println("If statement entered");
          }
        
        //35 //124
        if (card3 == card5 && card3 != card1 && card3 != card2 && card3 != card4 && card1 != card2 && card1 != card4 && card2 != card4){
            onePair++; 
          //System.out.println("If statement entered");
          }
        
        //45 //123
        if (card4 == card5 && card4 != card1 && card4 != card2 && card4 != card3 && card1 != card2 && card1 != card3 && card2 != card3){
            onePair++;   
          //System.out.println("If statement entered");
          }
        
        countHands++;
      }//big while loop  
      
      /*System.out.println(onePair);
      System.out.println(twoPair);
      System.out.println(threeOfKind);
      System.out.println(fourOfKind); 
      
      System.out.println("Count Hands: " + countHands); */
      
      double totalOnePair = (double) onePair / (double) numHands;
      double totalTwoPair = (double) twoPair / (double) numHands;
      double totalThree = (double) threeOfKind/ (double) numHands;
      double totalFour = (double) fourOfKind/ (double) numHands;
      
      System.out.println("The number of loops: " + numHands);
      System.out.println("The probability of Four-of-a-kind: " + totalFour);
      System.out.println("The probability of Three-of-a-kind: " + totalThree);
      System.out.println("The probability of Two-Pair: " + totalTwoPair);
      System.out.println("The probability of One-Pair: " + totalOnePair);
      
    }//ends main
  }//ends class

