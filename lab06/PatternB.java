///////////////
/// CSE02 PatternB Lab06
/// Ann Foley
/// 10/12/18
/// Section 110

//Coded using nested loops, displays pattern based on user input, eg. if user enters 6
/* 

1 2 3 4 5 6
1 2 3 4 5
1 2 3 4
1 2 3
1 2
1
*/

import java.util.Scanner;
  public class PatternB{
    public static void main(String[] args){
     
      Scanner myScanner = new Scanner( System.in );
  
      int intTest = 1; //declares a test variable to enter/exit the while statements  
      int numRows = 0;
      
      //tests user input
      while(intTest < 2){
     
      System.out.println("Enter an integer 1-10: ");
      boolean correctInt = myScanner.hasNextInt(); //checks to make sure the input is an integer
      
        if (correctInt){
          numRows = myScanner.nextInt(); 
        }
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer from 1-10");
        }//ends else
        
          if (numRows > 0 && numRows < 11){  
            intTest++; //increments test value to leave while statement
         }
          
          else  {
            myScanner.next();
            System.out.println("Error invalid input, please enter an integer from 1-10");
         }//en s else 
      
     }//ends while
      
      //this time,you want the first row to be numRows numbers long
      //so the first for loop is the columns
        for (int columns = numRows; columns >= 1; columns--){
          //starts each row with a one and increments r
          for (int r = 1; r <= columns; r++){ //rows
            System.out.print(r + " ");
        }
        System.out.println(); //prints new line between rows
        }// ends big for


      
    }//ends main
    
  }//ends class