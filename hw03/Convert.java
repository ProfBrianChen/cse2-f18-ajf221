///////////////
/// CSE02 Convert hw03
/// Ann Foley
/// 9/18/18
/// Section 110

import java.util.Scanner; //imports Scanner, must come before class

public class Convert{
  public static void main(String[] args){
    
    Scanner myScanner; //declares scanner
    myScanner = new Scanner(System.in); //constructs scanner
    
    System.out.print("Enter the affected area in acres: "); //promts user for input of acres
    double rainAcres = myScanner.nextDouble(); //declares the input as double and assigns variable
    
    System.out.print("Enter the rainfall in the affected area: "); //prompts user for input of inches of rain
    double rainInches = myScanner.nextDouble(); //declares the input as double and assigns variable
    
    final double galPerAcreInch = 27154; //conversion unit; gallons per acre inch
    final double cubicMilesPerGal = 9.08169e-13; //conversion unit; cubic miles per gallon
    
    //Calculations
    double rainAcreInches = rainAcres * rainInches; //converts acres and  rain in inches to rain in acre inches
    double rainGallons = galPerAcreInch * rainAcreInches; //converts rain in acre inches to gallons
    double rainCubicMiles = cubicMilesPerGal * rainGallons; //converts rain in gallons to cubic MIles
    
    System.out.println(rainCubicMiles + " cubic miles"); //prints the amount of rain in cubic miles
    
  } //ends main 
  
} //ends class