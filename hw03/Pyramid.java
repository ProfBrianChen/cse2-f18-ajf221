///////////////
/// CSE02 Convert hw03
/// Ann Foley
/// 9/18/18
/// Section 110

import java.util.Scanner; //imports

public class Pyramid{
  public static void main(String[] args){
    
    Scanner myScanner; //declares Scanner
    myScanner = new Scanner(System.in); //constructs Scanner
    
    System.out.print("The square side of the pyramid is (input length): "); //prompts user for input of lenght
    double lengthPyramid = myScanner.nextDouble(); //declares the input as double and assigns variable
    
    System.out.print("The height of the pyramid is (input height): "); //promts the user for input of height
    double heightPyramid = myScanner.nextDouble(); //declares the input as double and assigns variable
    
    //Calculations
    //calculates the volume of a pyramid using the formula lwh/3
    double volumePyramid = (Math.pow(lengthPyramid,2) * heightPyramid) / 3; 
    
    System.out.println("The volume inside the pyramid is: " + volumePyramid + "."); //prints the volume of the pyramid
    
  } //ends main
} //ends class