///////////////
/// CSE02 Shuffling HW08
/// Ann Foley
/// 11/14/18
/// Section 110

//prints a card deck
//prints a shuffled card deck
//prints a hand of a given number of cards

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class Shuffling{ 
public static void main(String[] args) { 

Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; 
String[] hand = new String[5]; 

    int intTest = 1; //declares a test variable to enter/exit the while statements  
    int numCards = 0;
      
      //tests user input
      while(intTest < 2){
      System.out.println("How many cards would you like in your hand (0-52)? ");
      boolean correctInt = scan.hasNextInt(); //checks to make sure the input is an integer
  
        if (correctInt){
          numCards = scan.nextInt(); 
          
          //checks input is within range
          if (numCards > 0 && numCards < 53){  
            intTest++; //increments test value to leave while statement
         }//ends inside if
          
        }//ends big if

        else {
            scan.next();
            System.out.println("Error invalid input, please enter an integer from 1-52");
         }//ends else 
      
     }//ends while

int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards, numCards); 
shuffle(cards); 
printArray(cards, numCards); 

while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand, numCards);
   index = index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt();  
}  
          
  
  }//main 

public static void printArray(String[] list, int numCards){
  //prints the full decks
  if(list.length == 52){
  for (int i=0; i<52; i++){
  System.out.print(list[i] +" ");
  }
  System.out.println();
  }
  
  //prints hand
  else{
    for (int i=0; i<numCards; i++){
    System.out.print(list[i] +" ");
  }
    
  System.out.println();
    
  }
}//print Array 

public static String[] shuffle(String[] list){
   Random randGen = new Random(); //random number generator
   System.out.println("Shuffled");
   //int count = 0;
        for(int i=0; i<52; i++){
          int r = randGen.nextInt(50)+1;//generates a random number 0-51
          String temp = list[0];//creates dummy to equal the string at 0
          list[0] = list[r];//sets string at 0 = string at r
          list[r] = temp;//sets r = original 0 value
         // count++;
        }     
       // System.out.println("COUNT: " +count);
        return list;
 }//shuffle
  
  public static String[] getHand(String[] list, int index, int numCards){
    String[] hand = new String[numCards];//declares another array for the hand of cards
    for (int i= 0; i < numCards; i++){
      hand[i] = list[index];//sets the hand index equal to the list at the last index
      index--; //chooses next card in deck
    }
    System.out.println("Hand");
    return hand;
  }//getHand

}//class



