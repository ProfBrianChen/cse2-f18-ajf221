///////////////
/// CSE02 UserInput Lab05
/// Ann Foley
/// 10/5/18
/// Section 110

//Your program is to write loops that asks the user to enter information
//relating to a course they are currently taking - ask for 
//the course number, department name, the number of times it meets in a week, 
//the time the class starts, the instructor name, and the number of students. 
//For each item, check to make sure that what the user enters is actually of the correct type,
//and if not, use an infinite loop to ask again. So if an integer needs to be entered,
//make sure an integer is entered, and loop until one actually is. Print Error if not

// course number, department, number of times a week, 
//time, instructors name, number of students

import java.util.Scanner;
  public class UserInput{
    public static void main(String[] args){
     
      Scanner myScanner = new Scanner( System.in );
      
      
      int intTest = 1; //declares a test variable to enter/exit the while statements
      //String name = "undefined";
      
      //Course Number
      
      while(intTest < 2){
     
      System.out.println("What is the course number? ");
      boolean correctInt = myScanner.hasNextInt(); //checks to make sure the input is an integer
      
        if (correctInt){
          int courseNum = myScanner.nextInt(); 
          intTest++; //increments test value to leave while statement
        
      }//ends if
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer");
      }//ends else
      
      }//ends while
      
      intTest--; //decreases test value to 1 so it can enter while statement 
      
      //Department Name
      
      while(intTest < 2){
      
      System.out.println("What is the department name? ");
      boolean correctName = myScanner.hasNext();
      
        if (correctName){
          String department= myScanner.next();
          intTest++;
         }//ends if
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter a string");
        }
       }//ends while
      
        //While loop for integer (number of times the class meets a week)
      
      intTest--;
      
      // times a week
      
      
      while(intTest < 2){
     
        System.out.println("how many times a week does the class meet? ");
        boolean correctInt = myScanner.hasNextInt();
      
        if (correctInt){
          int timesPerWeek = myScanner.nextInt();
          intTest++;
        
          }//ends if
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer");
          }
      
      }//ends while
      
      intTest--;
      
      //Time class meets
      
      while(intTest < 2){
     
      System.out.println("What time does the class meet? (do not include a colon) ");
      boolean correctInt = myScanner.hasNextInt();
      
        if (correctInt){
          int classTime = myScanner.nextInt();
          intTest++;
        
      }//ends if
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer (do not include a colon) ");
      }
      
      }//ends while
      
      intTest--;
      
      //Instructors Name
      
      while(intTest < 2){
      
      System.out.println("What is the instructor's name? ");
      boolean correctName = myScanner.hasNext();
      
        if (correctName){
          String name = myScanner.next();
          System.out.println(name);
          intTest++;
         }//ends if
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter a string");
        }
       }//ends while
      
      intTest--;
      
      //Number of Students
      
      while(intTest < 2){
     
      System.out.println("How many students are in the class? ");
      boolean correctInt = myScanner.hasNextInt();
      
        if (correctInt){
          int NumStudents = myScanner.nextInt();
          intTest++;
        
      }//ends if
        
        else {
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer");
      }
      
      }//ends while
       
    }//ends main
    
  }//ends class
