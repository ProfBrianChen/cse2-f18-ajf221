///////////////
/// CSE02 Cyclometer Lab02
/// Ann Foley
/// 9/7/18
/// Section 110

/*bicycle cyclometer records two kinds of data, the time elasped 
in seconds, and the number of rotations 
of the front wheel during that time. 

For two trips, given time and rotation count, 
your program should print the 
number of minutes, rotations (counts),the distance of each trip, and distance of the two trips combined */

public class Cyclometer {
  //main method required for every Java program
  public static void main (String[] args){
    
    //our input data
    
    int secsTrip1 = 480; //seconds to complete trip 1
    int secsTrip2 = 3220; //seconds to complete trip 2
    int countsTrip1 = 1561; //number of rotations of the wheel trip 1
    int countsTrip2 = 9037; //number of rotations of the wheel trip 1
    
    //our intermediate variables and output data
    
    double wheelDiameter = 27.0, //wheel diameter; all values following comma are also doubles
    PI = 3.14159, //value of pi 
    feetPerMile=5280,  //amount of feet in a mile
  	inchesPerFoot=12,   //amount of inches in a foot
  	secondsPerMinute=60;  //amount of seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  //declares distances as doubles 
    
    //print the number of seconds (converted to minutes) and the counts for each trip
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); //trip 1
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); //trip 2
    
    //Calculations 
    
    //for each count, a rotation of the wheel travels 
    //the diameter in inches times PI
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; //calculating distance in inches of trip 1
    distanceTrip1/= inchesPerFoot * feetPerMile; //divides answer above by inchesPerFoot * feetPerMile to get distance in miles
	  distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; //gives distanceTrip2 in miles combined in one step
    totalDistance = distanceTrip1 + distanceTrip2; //gives total distance

    //Print out the output data.
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");

    
    
  } //end of main method
} //end of class