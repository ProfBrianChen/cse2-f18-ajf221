///////////////
/// CSE02 EncriptedX HW06
/// Ann Foley
/// 10/24/18
/// Section 110

import java.util.Scanner;
  public class EncryptedX{
    public static void main(String[] args){
     
      Scanner myScanner = new Scanner( System.in );
  
      int intTest = 1; //declares a test variable to enter/exit the while statements  
      int input = 0;
      
      //tests user input
      while(intTest < 2){
      System.out.println("Enter an integer 1-100: ");
      boolean correctInt = myScanner.hasNextInt(); //checks to make sure the input is an integer
      
        if (correctInt){
          input = myScanner.nextInt(); 
          
          //checks input is within range
          if (input > 0 && input < 101){  
            intTest++; //increments test value to leave while statement
         }//ends inside if
          
        }//ends big if
        
        else if (input < 0 || input> 100){
          myScanner.next();
          System.out.println("Error invalid input, please enter an integer from 1-100");
        }//ends else if

        else {
            myScanner.next();
            System.out.println("Error invalid input, please enter an integer from 1-100");
         }//ends else 
      
     }//ends while
      
     //System.out.println("input:" + numRows); //test print statement
      
      //input = length/width
      //r= rows
      //p= position of spaces and stars
      
      for(int r=0; r <= input; r++){ 
      //keeps track of number of rows, increments until it is equal to the input
        
        for (int p=0; p <= input; p++){ //keeps track of # columns
          
          if(p==r || p == (input-r)){
            //prints spaces
            //left spaces are when p=r position
            //right spaces are when p= input-r
            System.out.print(" ");
           }//ends if
             
             else {
               //prints stars wherever p does not equal r
               System.out.print("*");
             }//ends else
             
          }//ends p for
        
           //prints new line after program completes a row
           System.out.println();  
           
      }//ends r for

    }//ends main
  }//ends class