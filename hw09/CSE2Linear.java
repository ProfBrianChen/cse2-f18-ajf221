///////////////
/// CSE02 Linear & Binary Search HW09
/// Ann Foley
/// 11/27/18
/// Section 110

//Gets user input of ascending grades 0-100
//Takes user input and does a binary search
//randomly shuffles array and does a lineary search
//both searches count the number of iterations

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear{ 
  public static void main(String[] args){ 
    
    Scanner myScanner = new Scanner(System.in);
    int [] array = new int[15];
    int intTest = 2;
    int input = 0;
    
    System.out.println("Enter 15 ascending ints for final grades in CSE2:");
    for(int i = 0; i < 15; i++){
      
      int h = 15 - i;//to be used to count the number of iterations left
      intTest--;
      
      while(intTest < 2){
      boolean correctInt = myScanner.hasNextInt(); //checks to make sure the input is an integer
      
        if (correctInt){ //if correct, program proceeds with tests
          input = myScanner.nextInt(); 
           
          
          if (input > -1 && input < 101){ //checks input is within range
            array[i] = input;
             intTest++;
            if(i > 0){//checks if all inputs after the first are greater than the previous
              //int j = i-1;
              if(array[i-1] > array[i]){           
                System.out.println("Error: Please input an integer greater than " + array[i-1] );
                i--; //increments test value to leave while statement
                
              }
              else{
                array[i] = input; 
              }//ends else
            }
          }
          
          else {
          //myScanner.next();
          System.out.println("Error: Please input an integer 0-100" );
          continue;
        }//ends else if
          
        }//ends big if

        else {
            myScanner.next();
            System.out.println("Error: Please input an integer");
            
         }//ends else 
      
     }//ends while
      //System.out.print(i + " ");  
   }//ends for loop
    
      //prints out array
      for(int i=0; i < array.length; i++){
      System.out.print(array[i]+ " ");
      }
      System.out.println();
      
      //BINARY SEARCH
      //used Prof Carr's example code
      System.out.println("Enter a grade to be searched for:");
      int key = myScanner.nextInt();
      
      int low = 0;
      int high = array.length-1;
      int iterations = 0;
      
      while(high >= low) {
        iterations++;//counts interations
        int mid = (low + high)/2;
          if (key < array[mid]) {
            high = mid - 1;
          }
          else if (key == array[mid]) {
            System.out.println(key + " was found with " + iterations + " iterations");
            break;
          }
          else {
            low = mid + 1;
          }
          
          if(high < low) {
            System.out.println(key + " was not found with " + iterations + " iterations");
            
          }//ends if
      }//ends while
      
       Random randGen = new Random(); //random number generator
       System.out.println("Scrambled");
   
      for(int i =0; i < array.length; i++){
        int r = randGen.nextInt(14);
        int tempVal = array[i];
        array[i] = array[r]; //copies each element of the array 
        array[r] = tempVal;
      }//ends for
      
      //prints out array
      for(int i=0; i < array.length; i++){
      System.out.print(array[i]+ " ");
      }
      System.out.println();

      //LINEAR SEARCH
      
      System.out.println("Enter a grade to be searched for:");
      key = myScanner.nextInt();
      
      for (int i = 0; i < array.length; i++) { 
        if (key == array[i]){ 
          System.out.println(key + " was found with " + i + " iterations");
          break;
        }
        if (i == 14){//if key was not found by lasy iterations, this if statement is entered
          i++;
          System.out.println(key + " was not found with " + i + " iterations");
        }
      }//ends linear for
      
  }//main
}//class