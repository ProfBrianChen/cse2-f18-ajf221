/* a literal is a constant value that appears directly in a program 
 int i = 34; //we say "the literal 34"
 
 the following are types of integers:
 - byte age = 43B;
 - short y = 5; 
 - long x = 100000;
 
 - boolean literals are true or false
 
 if you have data that is a constant value, declare it using final and capital letters
 - final double PI= 3.141596
 - you CANNOT change a final variable, or computer gives error

Math Methods:
Math.pow(arg1,arg2); // raises arg1 to the power of arg2
Math.ceil(arg); // rounds up to nearest integer, 2.9 => 3
Math.abs(arg); // absolute value */